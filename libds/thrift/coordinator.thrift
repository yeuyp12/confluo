namespace java edu.berkeley.cs.datastore
namespace cpp datastore
namespace py datastore

service coordinator_service {
  list<i64> get_snapshot(),
}
