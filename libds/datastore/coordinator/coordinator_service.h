/**
 * Autogenerated by Thrift Compiler (0.10.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
#ifndef coordinator_service_H
#define coordinator_service_H

#include <thrift/TDispatchProcessor.h>
#include <thrift/async/TConcurrentClientSyncInfo.h>
#include "coordinator/coordinator_types.h"

namespace datastore {

#ifdef _WIN32
  #pragma warning( push )
  #pragma warning (disable : 4250 ) //inheriting methods via dominance 
#endif

class coordinator_serviceIf {
 public:
  virtual ~coordinator_serviceIf() {}
  virtual void get_snapshot(std::vector<int64_t> & _return) = 0;
};

class coordinator_serviceIfFactory {
 public:
  typedef coordinator_serviceIf Handler;

  virtual ~coordinator_serviceIfFactory() {}

  virtual coordinator_serviceIf* getHandler(const ::apache::thrift::TConnectionInfo& connInfo) = 0;
  virtual void releaseHandler(coordinator_serviceIf* /* handler */) = 0;
};

class coordinator_serviceIfSingletonFactory : virtual public coordinator_serviceIfFactory {
 public:
  coordinator_serviceIfSingletonFactory(const boost::shared_ptr<coordinator_serviceIf>& iface) : iface_(iface) {}
  virtual ~coordinator_serviceIfSingletonFactory() {}

  virtual coordinator_serviceIf* getHandler(const ::apache::thrift::TConnectionInfo&) {
    return iface_.get();
  }
  virtual void releaseHandler(coordinator_serviceIf* /* handler */) {}

 protected:
  boost::shared_ptr<coordinator_serviceIf> iface_;
};

class coordinator_serviceNull : virtual public coordinator_serviceIf {
 public:
  virtual ~coordinator_serviceNull() {}
  void get_snapshot(std::vector<int64_t> & /* _return */) {
    return;
  }
};


class coordinator_service_get_snapshot_args {
 public:

  coordinator_service_get_snapshot_args(const coordinator_service_get_snapshot_args&);
  coordinator_service_get_snapshot_args& operator=(const coordinator_service_get_snapshot_args&);
  coordinator_service_get_snapshot_args() {
  }

  virtual ~coordinator_service_get_snapshot_args() throw();

  bool operator == (const coordinator_service_get_snapshot_args & /* rhs */) const
  {
    return true;
  }
  bool operator != (const coordinator_service_get_snapshot_args &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const coordinator_service_get_snapshot_args & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class coordinator_service_get_snapshot_pargs {
 public:


  virtual ~coordinator_service_get_snapshot_pargs() throw();

  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};

typedef struct _coordinator_service_get_snapshot_result__isset {
  _coordinator_service_get_snapshot_result__isset() : success(false) {}
  bool success :1;
} _coordinator_service_get_snapshot_result__isset;

class coordinator_service_get_snapshot_result {
 public:

  coordinator_service_get_snapshot_result(const coordinator_service_get_snapshot_result&);
  coordinator_service_get_snapshot_result& operator=(const coordinator_service_get_snapshot_result&);
  coordinator_service_get_snapshot_result() {
  }

  virtual ~coordinator_service_get_snapshot_result() throw();
  std::vector<int64_t>  success;

  _coordinator_service_get_snapshot_result__isset __isset;

  void __set_success(const std::vector<int64_t> & val);

  bool operator == (const coordinator_service_get_snapshot_result & rhs) const
  {
    if (!(success == rhs.success))
      return false;
    return true;
  }
  bool operator != (const coordinator_service_get_snapshot_result &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const coordinator_service_get_snapshot_result & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};

typedef struct _coordinator_service_get_snapshot_presult__isset {
  _coordinator_service_get_snapshot_presult__isset() : success(false) {}
  bool success :1;
} _coordinator_service_get_snapshot_presult__isset;

class coordinator_service_get_snapshot_presult {
 public:


  virtual ~coordinator_service_get_snapshot_presult() throw();
  std::vector<int64_t> * success;

  _coordinator_service_get_snapshot_presult__isset __isset;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);

};

class coordinator_serviceClient : virtual public coordinator_serviceIf {
 public:
  coordinator_serviceClient(boost::shared_ptr< ::apache::thrift::protocol::TProtocol> prot) {
    setProtocol(prot);
  }
  coordinator_serviceClient(boost::shared_ptr< ::apache::thrift::protocol::TProtocol> iprot, boost::shared_ptr< ::apache::thrift::protocol::TProtocol> oprot) {
    setProtocol(iprot,oprot);
  }
 private:
  void setProtocol(boost::shared_ptr< ::apache::thrift::protocol::TProtocol> prot) {
  setProtocol(prot,prot);
  }
  void setProtocol(boost::shared_ptr< ::apache::thrift::protocol::TProtocol> iprot, boost::shared_ptr< ::apache::thrift::protocol::TProtocol> oprot) {
    piprot_=iprot;
    poprot_=oprot;
    iprot_ = iprot.get();
    oprot_ = oprot.get();
  }
 public:
  boost::shared_ptr< ::apache::thrift::protocol::TProtocol> getInputProtocol() {
    return piprot_;
  }
  boost::shared_ptr< ::apache::thrift::protocol::TProtocol> getOutputProtocol() {
    return poprot_;
  }
  void get_snapshot(std::vector<int64_t> & _return);
  void send_get_snapshot();
  void recv_get_snapshot(std::vector<int64_t> & _return);
 protected:
  boost::shared_ptr< ::apache::thrift::protocol::TProtocol> piprot_;
  boost::shared_ptr< ::apache::thrift::protocol::TProtocol> poprot_;
  ::apache::thrift::protocol::TProtocol* iprot_;
  ::apache::thrift::protocol::TProtocol* oprot_;
};

class coordinator_serviceProcessor : public ::apache::thrift::TDispatchProcessor {
 protected:
  boost::shared_ptr<coordinator_serviceIf> iface_;
  virtual bool dispatchCall(::apache::thrift::protocol::TProtocol* iprot, ::apache::thrift::protocol::TProtocol* oprot, const std::string& fname, int32_t seqid, void* callContext);
 private:
  typedef  void (coordinator_serviceProcessor::*ProcessFunction)(int32_t, ::apache::thrift::protocol::TProtocol*, ::apache::thrift::protocol::TProtocol*, void*);
  typedef std::map<std::string, ProcessFunction> ProcessMap;
  ProcessMap processMap_;
  void process_get_snapshot(int32_t seqid, ::apache::thrift::protocol::TProtocol* iprot, ::apache::thrift::protocol::TProtocol* oprot, void* callContext);
 public:
  coordinator_serviceProcessor(boost::shared_ptr<coordinator_serviceIf> iface) :
    iface_(iface) {
    processMap_["get_snapshot"] = &coordinator_serviceProcessor::process_get_snapshot;
  }

  virtual ~coordinator_serviceProcessor() {}
};

class coordinator_serviceProcessorFactory : public ::apache::thrift::TProcessorFactory {
 public:
  coordinator_serviceProcessorFactory(const ::boost::shared_ptr< coordinator_serviceIfFactory >& handlerFactory) :
      handlerFactory_(handlerFactory) {}

  ::boost::shared_ptr< ::apache::thrift::TProcessor > getProcessor(const ::apache::thrift::TConnectionInfo& connInfo);

 protected:
  ::boost::shared_ptr< coordinator_serviceIfFactory > handlerFactory_;
};

class coordinator_serviceMultiface : virtual public coordinator_serviceIf {
 public:
  coordinator_serviceMultiface(std::vector<boost::shared_ptr<coordinator_serviceIf> >& ifaces) : ifaces_(ifaces) {
  }
  virtual ~coordinator_serviceMultiface() {}
 protected:
  std::vector<boost::shared_ptr<coordinator_serviceIf> > ifaces_;
  coordinator_serviceMultiface() {}
  void add(boost::shared_ptr<coordinator_serviceIf> iface) {
    ifaces_.push_back(iface);
  }
 public:
  void get_snapshot(std::vector<int64_t> & _return) {
    size_t sz = ifaces_.size();
    size_t i = 0;
    for (; i < (sz - 1); ++i) {
      ifaces_[i]->get_snapshot(_return);
    }
    ifaces_[i]->get_snapshot(_return);
    return;
  }

};

// The 'concurrent' client is a thread safe client that correctly handles
// out of order responses.  It is slower than the regular client, so should
// only be used when you need to share a connection among multiple threads
class coordinator_serviceConcurrentClient : virtual public coordinator_serviceIf {
 public:
  coordinator_serviceConcurrentClient(boost::shared_ptr< ::apache::thrift::protocol::TProtocol> prot) {
    setProtocol(prot);
  }
  coordinator_serviceConcurrentClient(boost::shared_ptr< ::apache::thrift::protocol::TProtocol> iprot, boost::shared_ptr< ::apache::thrift::protocol::TProtocol> oprot) {
    setProtocol(iprot,oprot);
  }
 private:
  void setProtocol(boost::shared_ptr< ::apache::thrift::protocol::TProtocol> prot) {
  setProtocol(prot,prot);
  }
  void setProtocol(boost::shared_ptr< ::apache::thrift::protocol::TProtocol> iprot, boost::shared_ptr< ::apache::thrift::protocol::TProtocol> oprot) {
    piprot_=iprot;
    poprot_=oprot;
    iprot_ = iprot.get();
    oprot_ = oprot.get();
  }
 public:
  boost::shared_ptr< ::apache::thrift::protocol::TProtocol> getInputProtocol() {
    return piprot_;
  }
  boost::shared_ptr< ::apache::thrift::protocol::TProtocol> getOutputProtocol() {
    return poprot_;
  }
  void get_snapshot(std::vector<int64_t> & _return);
  int32_t send_get_snapshot();
  void recv_get_snapshot(std::vector<int64_t> & _return, const int32_t seqid);
 protected:
  boost::shared_ptr< ::apache::thrift::protocol::TProtocol> piprot_;
  boost::shared_ptr< ::apache::thrift::protocol::TProtocol> poprot_;
  ::apache::thrift::protocol::TProtocol* iprot_;
  ::apache::thrift::protocol::TProtocol* oprot_;
  ::apache::thrift::async::TConcurrentClientSyncInfo sync_;
};

#ifdef _WIN32
  #pragma warning( pop )
#endif

} // namespace

#endif
