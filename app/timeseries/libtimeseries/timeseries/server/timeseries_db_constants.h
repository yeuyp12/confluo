/**
 * Autogenerated by Thrift Compiler (0.10.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
#ifndef timeseries_db_CONSTANTS_H
#define timeseries_db_CONSTANTS_H

#include "server/timeseries_db_types.h"

namespace timeseries {

class timeseries_dbConstants {
 public:
  timeseries_dbConstants();

};

extern const timeseries_dbConstants g_timeseries_db_constants;

} // namespace

#endif
