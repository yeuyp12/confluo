#ifndef MONOLOG_DATALOG_H_
#define MONOLOG_DATALOG_H_

#include "monolog.h"

namespace monolog {

typedef monolog_linear_base <uint8_t> datalog;

}

#endif  // MONOLOG_DATALOG_H_
