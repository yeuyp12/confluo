#
# Autogenerated by Thrift Compiler (0.10.0)
#
# DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
#
#  options string: py
#

from thrift.Thrift import TType, TMessageType, TFrozenDict, TException, TApplicationException
from thrift.protocol.TProtocol import TProtocolException
import sys

from thrift.transport import TTransport


class TNode(object):
    """
    Attributes:
     - id
     - type
     - data
    """

    thrift_spec = (
        None,  # 0
        (1, TType.I64, 'id', None, None, ),  # 1
        (2, TType.I64, 'type', None, None, ),  # 2
        (3, TType.STRING, 'data', 'UTF8', None, ),  # 3
    )

    def __init__(self, id=None, type=None, data=None,):
        self.id = id
        self.type = type
        self.data = data

    def read(self, iprot):
        if iprot._fast_decode is not None and isinstance(iprot.trans, TTransport.CReadableTransport) and self.thrift_spec is not None:
            iprot._fast_decode(self, iprot, (self.__class__, self.thrift_spec))
            return
        iprot.readStructBegin()
        while True:
            (fname, ftype, fid) = iprot.readFieldBegin()
            if ftype == TType.STOP:
                break
            if fid == 1:
                if ftype == TType.I64:
                    self.id = iprot.readI64()
                else:
                    iprot.skip(ftype)
            elif fid == 2:
                if ftype == TType.I64:
                    self.type = iprot.readI64()
                else:
                    iprot.skip(ftype)
            elif fid == 3:
                if ftype == TType.STRING:
                    self.data = iprot.readString().decode('utf-8') if sys.version_info[0] == 2 else iprot.readString()
                else:
                    iprot.skip(ftype)
            else:
                iprot.skip(ftype)
            iprot.readFieldEnd()
        iprot.readStructEnd()

    def write(self, oprot):
        if oprot._fast_encode is not None and self.thrift_spec is not None:
            oprot.trans.write(oprot._fast_encode(self, (self.__class__, self.thrift_spec)))
            return
        oprot.writeStructBegin('TNode')
        if self.id is not None:
            oprot.writeFieldBegin('id', TType.I64, 1)
            oprot.writeI64(self.id)
            oprot.writeFieldEnd()
        if self.type is not None:
            oprot.writeFieldBegin('type', TType.I64, 2)
            oprot.writeI64(self.type)
            oprot.writeFieldEnd()
        if self.data is not None:
            oprot.writeFieldBegin('data', TType.STRING, 3)
            oprot.writeString(self.data.encode('utf-8') if sys.version_info[0] == 2 else self.data)
            oprot.writeFieldEnd()
        oprot.writeFieldStop()
        oprot.writeStructEnd()

    def validate(self):
        return

    def __repr__(self):
        L = ['%s=%r' % (key, value)
             for key, value in self.__dict__.items()]
        return '%s(%s)' % (self.__class__.__name__, ', '.join(L))

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

    def __ne__(self, other):
        return not (self == other)


class TLink(object):
    """
    Attributes:
     - version
     - id1
     - link_type
     - id2
     - time
     - data
    """

    thrift_spec = (
        None,  # 0
        (1, TType.I64, 'version', None, None, ),  # 1
        (2, TType.I64, 'id1', None, None, ),  # 2
        (3, TType.I64, 'link_type', None, None, ),  # 3
        (4, TType.I64, 'id2', None, None, ),  # 4
        (5, TType.I64, 'time', None, None, ),  # 5
        (6, TType.STRING, 'data', 'UTF8', None, ),  # 6
    )

    def __init__(self, version=None, id1=None, link_type=None, id2=None, time=None, data=None,):
        self.version = version
        self.id1 = id1
        self.link_type = link_type
        self.id2 = id2
        self.time = time
        self.data = data

    def read(self, iprot):
        if iprot._fast_decode is not None and isinstance(iprot.trans, TTransport.CReadableTransport) and self.thrift_spec is not None:
            iprot._fast_decode(self, iprot, (self.__class__, self.thrift_spec))
            return
        iprot.readStructBegin()
        while True:
            (fname, ftype, fid) = iprot.readFieldBegin()
            if ftype == TType.STOP:
                break
            if fid == 1:
                if ftype == TType.I64:
                    self.version = iprot.readI64()
                else:
                    iprot.skip(ftype)
            elif fid == 2:
                if ftype == TType.I64:
                    self.id1 = iprot.readI64()
                else:
                    iprot.skip(ftype)
            elif fid == 3:
                if ftype == TType.I64:
                    self.link_type = iprot.readI64()
                else:
                    iprot.skip(ftype)
            elif fid == 4:
                if ftype == TType.I64:
                    self.id2 = iprot.readI64()
                else:
                    iprot.skip(ftype)
            elif fid == 5:
                if ftype == TType.I64:
                    self.time = iprot.readI64()
                else:
                    iprot.skip(ftype)
            elif fid == 6:
                if ftype == TType.STRING:
                    self.data = iprot.readString().decode('utf-8') if sys.version_info[0] == 2 else iprot.readString()
                else:
                    iprot.skip(ftype)
            else:
                iprot.skip(ftype)
            iprot.readFieldEnd()
        iprot.readStructEnd()

    def write(self, oprot):
        if oprot._fast_encode is not None and self.thrift_spec is not None:
            oprot.trans.write(oprot._fast_encode(self, (self.__class__, self.thrift_spec)))
            return
        oprot.writeStructBegin('TLink')
        if self.version is not None:
            oprot.writeFieldBegin('version', TType.I64, 1)
            oprot.writeI64(self.version)
            oprot.writeFieldEnd()
        if self.id1 is not None:
            oprot.writeFieldBegin('id1', TType.I64, 2)
            oprot.writeI64(self.id1)
            oprot.writeFieldEnd()
        if self.link_type is not None:
            oprot.writeFieldBegin('link_type', TType.I64, 3)
            oprot.writeI64(self.link_type)
            oprot.writeFieldEnd()
        if self.id2 is not None:
            oprot.writeFieldBegin('id2', TType.I64, 4)
            oprot.writeI64(self.id2)
            oprot.writeFieldEnd()
        if self.time is not None:
            oprot.writeFieldBegin('time', TType.I64, 5)
            oprot.writeI64(self.time)
            oprot.writeFieldEnd()
        if self.data is not None:
            oprot.writeFieldBegin('data', TType.STRING, 6)
            oprot.writeString(self.data.encode('utf-8') if sys.version_info[0] == 2 else self.data)
            oprot.writeFieldEnd()
        oprot.writeFieldStop()
        oprot.writeStructEnd()

    def validate(self):
        return

    def __repr__(self):
        L = ['%s=%r' % (key, value)
             for key, value in self.__dict__.items()]
        return '%s(%s)' % (self.__class__.__name__, ', '.join(L))

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

    def __ne__(self, other):
        return not (self == other)
